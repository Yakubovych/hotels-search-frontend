# Frontend

---

## Documentation

* [Requirements](#requirements)
* [Get Start](#get-start)
* [Configure app](#configure-app)
* [Run Project](#run-project)
* [Result](#check-result)


## Requirements

For development, you will only need Node.js

    $ node --version
    v10.16.x

    $ npm --version
    6.10.x

## Get Start

    $ git clone https://gitlab.com/Yakubovych/hotels-search-frontend.git
    $ cd <folder_name>
    $ npm i

## Run project

    $ npm start

## **Check result**

Open `http://localhost:3000` in your browser

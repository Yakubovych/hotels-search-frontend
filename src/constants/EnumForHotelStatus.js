export const HOTEL_STATUS = {    
    ACTIVATED: "activated",
    INACTIVE:  "inactive",
    REJECT:    "reject",
    WAITINGFORAPPROVAL: "waiting for approval"
};
  
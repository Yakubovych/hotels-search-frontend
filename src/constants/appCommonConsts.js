// here must be placed constans that used in different parts of application 
const OWNER_HOTELS = 'Owner`s hotels';

export const HOTEL_MANAGEMENT_SUBPAGE = {
    DEFAULT: OWNER_HOTELS,
    OWNER_HOTELS: OWNER_HOTELS,
    CREATE_HOTEL: 'Create hotel',
    ROOMS: 'Rooms',
    PHOTOS: 'Photos',
    OTHER: 'Other'
};

export const UNKNOWN_HOTEL = {
    id: 1,
    name: '',
    status: "INACTIVE",
    address: {
        city: '',
        street: '',
        house: 0
    },
    rooms: [],
    photos: []
};

// here must be placed api urls 

export const HOST = "http://localhost:8000/api";
export const COUNT  = "/count";
export const SUSPEND = "/suspend";
export const OWNER ="/owner";
export const HOTEL_ID_PARAM = "hotelId";
export const HOTEL_ROOMS ="/rooms";
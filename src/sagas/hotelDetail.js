import { call, put, takeEvery} from 'redux-saga/effects';
import {
    REQUEST_GET_HOTELDETAILS,
    RECEIVE_GET_HOTELDETAILS,
    REQUEST_GET_FEEDBACK_BY_HOTEL_ID, RECEIVE_GET_FEEDBACK_BY_HOTEL_ID
}
    from '../constants/actionTypes';
import {getHotelDetails, getFeedbackByHotelId } from '../api/hotelDetailsApi';


function* workerGetHotelDetails(action){
    const id = action.payload;

    try {
        const response = yield call(getHotelDetails, id);
        yield put({type: RECEIVE_GET_HOTELDETAILS, payload: response.data.data});
    } catch (e){
        console.error(e);
    }
}

export function* watcherGetHotelDetails() {
    yield takeEvery(REQUEST_GET_HOTELDETAILS, workerGetHotelDetails)
}

function* workerGetFeedbackByHotelId(action){
    const {id, page, star} = action.payload;
    try {
        const response = yield call(getFeedbackByHotelId, id, page, star);
        const feedbackDetails = {
            feedback: response.data.data[0].feedback,
            count: response.data.data[0].feedbackCount[0].count,
        }
        yield put({type: RECEIVE_GET_FEEDBACK_BY_HOTEL_ID, payload: feedbackDetails});
    } catch (e){
        console.error(e);
    }
}

export function* watcherGetFeedbackByHotelId() {
    yield takeEvery(REQUEST_GET_FEEDBACK_BY_HOTEL_ID, workerGetFeedbackByHotelId)
}




import { call, put, takeEvery } from "redux-saga/effects";
import { REQUEST_CURRENT_PAGE, LOADING } from "../constants/actionTypes";
import { getHotels } from "../api/hotelApi";
import { receiveCurrentPage } from "../actions/hotelAction";
import { browserHistory } from "react-router";

export function* workerGetHotels(action) {
  try {
    const response = yield call(getHotels, action.payload);
    const currentPageDetails = {
      hotels: response[0].hotelsList,
      currentPage: action.payload,
      pageCount: response[0].pageCount
    };

    yield put(receiveCurrentPage(currentPageDetails));

    yield browserHistory.push(`/home/page/${action.payload}`);

    yield put({ type: LOADING });
    
  } catch (e) {
    console.error(e);
  }
}

export function* watcherGetHotels() {
  yield takeEvery(REQUEST_CURRENT_PAGE, workerGetHotels);
}

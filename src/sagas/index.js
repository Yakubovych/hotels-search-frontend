import { all, fork } from "redux-saga/effects";
import uploadSaga from "./uploadSaga";
import { watcherGetHotels } from "./hotelSaga";
import { watcherGetHotelDetails, watcherGetFeedbackByHotelId } from "./hotelDetail";
import { watcherGetHotelComforts, watcherDeleteComforts, watcherCreateComfort } from "./comfortsSaga";
import { watcherGetOrders } from "./orderSaga";
import { watcherSearchHotels } from "./searchSaga";
import { watcherCreateHotel } from "./createHotelSaga";
import { watcherGetInactiveHotels } from "./adminSaga";
import {
  watcherGetOAuthGoogle,
  watcherGetSignIn,
  watcherGetSignUp,
  watcherGetCheckAuth,
  watcherGetSignOut,
  watcherDispatchAuthSignedIn,
  watcherGetUserData,
  watcherGetDashboard,
  watcherGetEditProfile,
  watcherVerifyUser,
  watcherGetVerificationEmail,
  watcherGetChangePassword,
  watcherGetValidationPassword,
  watcherGetSendRecoveryEmail,
  watcherRecoverPassword,
} from "./authSaga";
import { watcherGetUserOrders, watcherGetUserFeedbacks, watcherAddUserFeedback } from "./feedbacksSaga";
import { watcherGetPayForOrder, watcherGetUserCabinetCreatedOrders, watcherGetUserCabinetPaidOrders, watcherGetUserCabinetCompletedOrders } from "./listOfUserOrdersSaga";
import { watcherGetHotelRooms } from "./manageHotelRoomsSaga";
import { watcherGetListOwnerHotels } from "./hotelManagementSaga";


export default function* rootSaga() {
  yield all([
    fork(watcherGetHotels),
    fork(watcherGetHotelDetails),
    fork(watcherGetFeedbackByHotelId),
    fork(watcherGetOrders),
    fork(watcherSearchHotels),
    fork(watcherCreateHotel),
    fork(watcherGetOAuthGoogle),
    fork(watcherGetSignIn),
    fork(watcherGetSignUp),
    fork(watcherGetCheckAuth),
    fork(watcherGetSignOut),
    fork(watcherDispatchAuthSignedIn),
    fork(watcherGetUserData),
    fork(watcherGetDashboard),
    fork(watcherGetEditProfile),
    fork(watcherVerifyUser),
    fork(watcherGetVerificationEmail),
    fork(watcherGetUserOrders),
    fork(watcherGetUserFeedbacks),
    fork(watcherAddUserFeedback),
    fork(watcherGetInactiveHotels),
    fork(watcherGetChangePassword),
    fork(watcherGetUserOrders),
    fork(watcherGetSendRecoveryEmail),
    fork(watcherRecoverPassword),
    fork(watcherGetValidationPassword),
    fork(uploadSaga),
    fork(watcherGetUserCabinetCreatedOrders),
    fork(watcherGetUserCabinetPaidOrders),
    fork(watcherGetUserCabinetCompletedOrders),
    fork(watcherGetPayForOrder),
    fork(watcherGetHotelComforts),
    fork(watcherDeleteComforts),
    fork(watcherCreateComfort),
    fork(watcherGetHotelRooms),
    fork(watcherGetListOwnerHotels)
  ]);
}

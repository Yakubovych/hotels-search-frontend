import { call, put, takeEvery} from 'redux-saga/effects';
import {
    REQUEST_GET_LIST_ROOMS_IN_ONE_HOTEL,
} from '../constants/actionTypes';
import {requestGetListRoomsInHotel} from '../api/manageHotelRoomsApi';
import * as actionCreator from "../actions/manageHotelRoomsAction";

function* workerGetHotelRooms(action){
    const hotelId = action.payload;
    try {
        const response = yield call(requestGetListRoomsInHotel, hotelId);
        yield put(actionCreator.receiveResponseGetListRoomsInOneHotel(response));
    } catch (e){
        console.error(e);
    }
}

export function* watcherGetHotelRooms() {
    yield takeEvery(REQUEST_GET_LIST_ROOMS_IN_ONE_HOTEL, workerGetHotelRooms)
}

import { call, put, takeEvery} from 'redux-saga/effects';
import {
    REQUEST_GET_LIST_OWNER_HOTELS,
} from '../constants/actionTypes';
import {requestGetListOwnerHotels} from '../api/hotelManagementApi';
import * as actionCreator from "../actions/hotelManagementAction";

function* workerGetListOwnerHotels(action){
    const hotelId = action.payload;
    try {
        const response = yield call(requestGetListOwnerHotels, hotelId);
        yield put(actionCreator.receiveResponseGetListOwnerHotels(response));
    } catch (e){
        console.error(e);
    }
}

export function* watcherGetListOwnerHotels() {
    yield takeEvery(REQUEST_GET_LIST_OWNER_HOTELS, workerGetListOwnerHotels)
}
import { call, put, takeEvery } from "redux-saga/effects";
import {
  REQUEST_SEARCH_HOTELS, REQUEST_SEARCH_TIPS  
} from "../constants/actionTypes";
import { searchHotels, searchTips } from "../api/searchApi";
import { receiveCurrentPage} from "../actions/hotelAction";
import { receiveSearchTips} from "../actions/searchAction";
 
function* workerSearchHotels(action) {
  try {
    const response = yield call(searchHotels, action.payload);
    const currentPageDetails = {
      hotels: response[0].hotelsList,
      currentPage: action.payload.currentPage,
      pageCount: response[0].pageCount
    };
    yield put(receiveCurrentPage(currentPageDetails));
  } catch (e) {
    console.error(e);
  }
}

function* workerSearchTips(action) {
  try {
    const response = yield call(searchTips, action.payload);
    yield put(receiveSearchTips(response));
  } catch (e) {
    console.error(e);
  }
}

export function* watcherSearchHotels() {
  yield takeEvery(REQUEST_SEARCH_HOTELS, workerSearchHotels);
  yield takeEvery(REQUEST_SEARCH_TIPS, workerSearchTips);
  
}

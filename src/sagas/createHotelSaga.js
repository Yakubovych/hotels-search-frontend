import { call, put, takeEvery } from "redux-saga/effects";
import { REQUEST_POST_CREATE_HOTEL } from "../constants/actionTypes";
import * as actionCreator from "../actions/createHotelAction";
import { createHotel } from "../api/createHotelApi";

function* workerCreateHotel(action) {
    const hotel = action.payload;
    try {
        const response =  yield call(createHotel, hotel);
        yield put(actionCreator.receiveResponsePOSTCreateHotel(response.data.message));
    } catch (e) {
        console.error(e);
    }
}

export function* watcherCreateHotel() {
    yield takeEvery(REQUEST_POST_CREATE_HOTEL, workerCreateHotel);
}


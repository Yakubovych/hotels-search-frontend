import { call, put, takeEvery, select } from "redux-saga/effects";
import {
  RECEIVE_GET_INACTIVE_HOTELS,
  REQUEST_GET_INACTIVE_HOTELS,
  RECEIVE_UPDATE_STATUS_HOTEL,
  REQUEST_UPDATE_STATUS_HOTEL,
  RECEIVE_GET_LIST_ACTIVE_HOTELS,
  REQUEST_GET_LIST_ACTIVE_HOTELS,
  RECEIVE_SEND_OWNER_MESSAGE,
  REQUEST_SEND_OWNER_MESSAGE,
  REQUEST_GET_COUNT_INACTIVE_HOTELS,
  RECEIVE_GET_COUNT_INACTIVE_HOTELS,
  RECEIVE_GET_LIST_SUSPEND_HOTELS,
  REQUEST_GET_LIST_SUSPEND_HOTELS,
  LOADING
} from "../constants/actionTypes";
import {
  getInactiveHotels,
  updateStatusHotel,
  getListActiveHolels,
  sendOwnerMessage,
  getCountInactiveHotels,
  getListSuspendHolels
} from "../api/adminApi";

const getAdminState = state => state.adminReducer;
const getAdminForm = state => state.form["sendOwnerMessage"];

function* workerGetInactiveHotels(action) {
  try {
    const response = yield call(getInactiveHotels);
    yield put({ type: RECEIVE_GET_INACTIVE_HOTELS, payload: response });
    yield put({ type: LOADING });

  } catch (e) {
    console.error(e);
  }
};

function* workerUpdateStatusHotel(action) {
  try {
    const adminState = yield select(getAdminState);

    yield call(updateStatusHotel, action.payload);
    yield put({
      type: RECEIVE_UPDATE_STATUS_HOTEL,
      payload: {
        hotels: adminState.notActiveHotels,
        activateHotelId: action.payload.id
      }
    });
  } catch (e) {
    console.error(e);
  }
};

function* workerGetListActiveHotels(acton) {
  try {
    const response = yield call(getListActiveHolels);

    yield put({ type: RECEIVE_GET_LIST_ACTIVE_HOTELS, payload: response });
    yield put({ type: LOADING });

  } catch (e) {
    console.error(e);
  }
};

function* workerSendOwnerMessage(action) {
  try {
    const state = yield select(getAdminState);
    const adminForm = yield select(getAdminForm);
    const response = yield call(sendOwnerMessage, action.payload);
    yield put({ type: RECEIVE_SEND_OWNER_MESSAGE, payload: response });
  } catch (e) {
    console.error(e);
  }
};

function* workerGetCountInactiveHotels(action) {
  try {
    const response = yield call(getCountInactiveHotels);

    yield put({ type: RECEIVE_GET_COUNT_INACTIVE_HOTELS, payload: response });
    
    yield put({ type: LOADING });

  } catch (e) {
    console.error(e);
  }
};

function* workerGetListSuspendHotels(acton) {
  try {
    const response = yield call(getListSuspendHolels);
    yield put({ type: RECEIVE_GET_LIST_SUSPEND_HOTELS, payload: response });

    yield put({ type: LOADING });

  } catch (e) {
    console.error(e);
  }
};

export function* watcherGetInactiveHotels() {
  yield takeEvery(REQUEST_GET_INACTIVE_HOTELS, workerGetInactiveHotels);
  yield takeEvery(REQUEST_UPDATE_STATUS_HOTEL, workerUpdateStatusHotel);
  yield takeEvery(REQUEST_GET_LIST_ACTIVE_HOTELS, workerGetListActiveHotels);
  yield takeEvery(REQUEST_SEND_OWNER_MESSAGE, workerSendOwnerMessage);
  yield takeEvery(
    REQUEST_GET_COUNT_INACTIVE_HOTELS,
    workerGetCountInactiveHotels
  );
  yield takeEvery(REQUEST_GET_LIST_SUSPEND_HOTELS, workerGetListSuspendHotels);
};

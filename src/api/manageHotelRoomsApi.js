import axios from "axios";
import { HOST, HOTEL_ROOMS, HOTEL_ID_PARAM } from "../constants/apiUrl";

export const requestGetListRoomsInHotel = async (hotelId) => {
    const response = await axios.get(`${HOST}${HOTEL_ROOMS}/?${HOTEL_ID_PARAM}=${hotelId}`);
    return response.data.data;
};

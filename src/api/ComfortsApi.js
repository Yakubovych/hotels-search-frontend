import axios from "axios";
import {HOST, HOTELS_COMFORTS_URL} from "../constants/actionTypes";

export const getHotelComforts = async (id) => {
    return await axios.get(`${HOST}/${HOTELS_COMFORTS_URL}/${id}`);
};

export const postComforts = async comfort => {
    return await axios.post(`${HOST}/${HOTELS_COMFORTS_URL}`, comfort);
};

export const deleteComforts = async (room_id,comfort_id) => {
    return await axios.delete(`${HOST}/${HOTELS_COMFORTS_URL}?room_id=${room_id}&comfort_id=${comfort_id}`);
};

import axios from "axios";
import { HOST, OWNER } from "../constants/apiUrl";

export const requestGetListOwnerHotels = async (hotelId) => {
    const response = await axios.get(`${HOST}${OWNER}/${hotelId}`);
    return response.data.data;
};
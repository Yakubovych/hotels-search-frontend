import axios from "axios";
import {HOST, HOTEL_URL, SEARCH_URL, HOTELS_FEEDBACK_URL} from '../constants/actionTypes';

export const getHotelDetails = async (id) => {
    return await axios.get(`${HOST}/${HOTEL_URL}/${id}`);
};

export const getFeedbackByHotelId= async (id,page,star) => {
    star = star ? star : "";
    return await axios.get(`${HOST}/${HOTELS_FEEDBACK_URL}/${SEARCH_URL}?id=${id}&page=${page}&star=${star}`);
};









import axios from "axios";
import { HOST, HOTEL_URL } from "../constants/actionTypes";

export const createHotel = async (hotel) => {
    return await axios.post(`${HOST}/${HOTEL_URL}/`, hotel);
};

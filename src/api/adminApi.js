import axios from "axios";
import {
  HOST,
  ADMIN,
  STATUS,
  HOTEL,
  INACTIVE,
  HOTEL_URL,
  ACTIVE,
  FORM,
  EMAIL,
} from "../constants/actionTypes";
import { COUNT, SUSPEND, OWNER } from "../constants/apiUrl";

export const getInactiveHotels = async () => {
  const response = await axios.get(`${HOST}${ADMIN}/${HOTEL_URL}${INACTIVE}`);
  return response.data.data;
};

export const updateStatusHotel = async data => {
  const response = await axios.put(
    `${HOST}${ADMIN}${STATUS}${HOTEL}/${data.id}`,
    data
  );
  return response.data.data;
};

export const getListActiveHolels = async () => {
  const response = await axios.get(`${HOST}${ADMIN}/${HOTEL_URL}${ACTIVE}`);
  return response.data.data;
};

export const sendOwnerMessage = async data => {
  const response = await axios.post(`${HOST}${ADMIN}${FORM}${EMAIL}`, data)
  return response.data;
};

export const getCountInactiveHotels = async () => {

  const response = await axios.get(`${HOST}${ADMIN}/${HOTEL_URL}${INACTIVE}${COUNT}`);
  return response.data.data;
};

export const getListSuspendHolels = async () => {
  const response = await axios.get(`${HOST}${ADMIN}/${HOTEL_URL}${SUSPEND}`);
  return response.data.data;
};

import axios from "axios";
import {
    HOST,
    ADMIN,
    HOTEL_URL,
    ACTIVE
} from "../constants/actionTypes";

export const getListActiveHolels = async () => {
    const response = await axios.get(`${HOST}${ADMIN}/${HOTEL_URL}${ACTIVE}`);
    return response.data.data;
}
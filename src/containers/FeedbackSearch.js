import {connect} from 'react-redux';
import FeedbackSearch from '../components/HotelDetails/FeedbackSearch.jsx';
import {
   requestGetFeedbackByHotelId,

} from '../actions/hotelAction';

function mapStateToProps(state) {
    return {
        feedback: state.hotelDetailsReducer.feedbackByHotelId,
        feedbackCount: state.hotelDetailsReducer.feedbackCount,
    }
};

function mapDispatchToProps(dispatch) {
    return{
        requestGetFeedbackByHotelId: (id,page,star) => dispatch(requestGetFeedbackByHotelId(id,page,star)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FeedbackSearch);


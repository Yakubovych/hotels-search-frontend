import { connect } from "react-redux";
import {
  requestSignOut,
  requestVerificationEmail
} from "../actions/authAction";
import DashboardHome from "../components/Dashboard/Dashboard.jsx";
import { inactiveStatusHotels, getListActiveHolels, getCountInactiveHotels} from "../actions/adminAction";

function mapStateToProps(state) {
  return {
    user: state.dash.user,
    countInactiveHotels: state.adminReducer.countInactiveHotels,
    orders: state.userCabinetOrders.orders,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getCountInactiveHotels: () => dispatch(getCountInactiveHotels()),
    inactiveStatusHotels: () => dispatch(inactiveStatusHotels()),
    getListActiveHolels: () => dispatch(getListActiveHolels()),
    requestSignOut: () => dispatch(requestSignOut()),
    requestVerificationEmail: params =>
      dispatch(requestVerificationEmail(params))
  };
}

const Dashboard = connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardHome);
export default Dashboard;
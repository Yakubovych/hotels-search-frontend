import { connect } from 'react-redux';
import ManageHotelRooms from '../components/ManageHotelRooms/ManageHotelRooms.jsx';
import {
    requestGetListRoomsInHotel,
    receiveResponseGetListRoomsInOneHotel
} from '../actions/manageHotelRoomsAction';

function mapStateToProps(state) {
    return {
        userId: state.dash.user.id,
        // selectedHotel: state.hotelManagementReducer.selectedHotel,
        // selectedHotelId: state.hotelManagementReducer.selectedHotel.id,
        selectedHotelRooms: state.manageHotelRoomsReducer.selectedHotelRooms
    }
}

function mapDispatchToProps(dispatch) {
    return {
        requestGetListRoomsInHotel: (params) => dispatch(
            requestGetListRoomsInHotel(params)
        ),
        receiveResponseGetListRoomsInOneHotel: (params) => dispatch(
            receiveResponseGetListRoomsInOneHotel(params)
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageHotelRooms);

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AdminComponent } from "../components/AdminComponent/AdminComponent.jsx";
import {
  inactiveStatusHotels,
  updateStatusHotel,
  getListActiveHolels,
  getCountInactiveHotels,
  getListSuspendHolels
} from "../actions/adminAction";

function mapStateToProps(state) {
  return {
    inactiveStatusHotels: state.inactiveStatusHotels,
    notActiveHotels: state.adminReducer.notActiveHotels,
    countNotActiveHotels: state.adminReducer.countNotActiveHotels,
    updateStatusHotel: state.adminReducer.inactiveStatusHotels,
    listActiveHotels: state.adminReducer.listActiveHotels,
    countActiveHotels: state.adminReducer.countActiveHotels,
    countInactiveHotels: state.adminReducer.countInactiveHotels,
    listSuspendHotels: state.adminReducer.listSuspendHotels,
    countSuspendHotels: state.adminReducer.countSuspendHotels,
    loading: state.adminReducer.loading
  };
}

function mapDispatchToProps(dispatch) {
  return {
    inactiveStatusHotels: bindActionCreators(inactiveStatusHotels, dispatch),
    updateStatusHotel: bindActionCreators(updateStatusHotel, dispatch),
    getListActiveHolels: bindActionCreators(getListActiveHolels, dispatch),
    getCountInactiveHotels: bindActionCreators(getCountInactiveHotels, dispatch),
    getListSuspendHolels: bindActionCreators(getListSuspendHolels, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminComponent);
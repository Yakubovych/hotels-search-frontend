import { connect } from "react-redux";
import { Search } from "../components/Search/Search.jsx";
import {
  requestSearchHotels,
  inputSearchString,
  inputRoomType,
  selectStartDate,
  selectFinishDate,
  clearSearchInputs,
  requestSearchTips
} from "../actions/searchAction";

function mapStateToProps(state) {
  return {
    hotels: state.hotels.SearchlistHotels,
    currentPage: state.hotels.currentPage,
    searchTips: state.hotels.searchTips,
    searchData: {
      SearchCity: state.hotels.inputSearchString,
      SearchRoomType: state.hotels.inputRoomType,
      StartDate: state.hotels.selectStartDate,
      FinishDate: state.hotels.selectFinishDate,
    }
  };
}

function mapDispatchToProps(dispatch) {
  return {
    requestSearchHotels: params => dispatch(requestSearchHotels(params)),
    requestSearchTips: params => dispatch(requestSearchTips(params)),
    inputSearchString: params => dispatch(inputSearchString(params)),
    inputRoomType: params => dispatch(inputRoomType(params)),
    selectStartDate: params => dispatch(selectStartDate(params)),
    selectFinishDate: params => dispatch(selectFinishDate(params)),
    clearSearchInputs: () => dispatch(clearSearchInputs())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);



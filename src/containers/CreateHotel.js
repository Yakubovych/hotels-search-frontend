import { connect } from 'react-redux';
import CreateHotel from '../components/CreateHotel/CreateHotel.jsx';
import {
    requestPOSTCreateHotel,
    receiveResponsePOSTCreateHotel,
} from '../actions/createHotelAction';

function mapStateToProps(state) {
    return {
        hotel: state.createHotelReducer.hotel
    }
}

function mapDispatchToProps(dispatch) {
    return {
        requestPOSTCreateHotel: (params) => dispatch(
            requestPOSTCreateHotel(params)
        ),
        receiveResponsePOSTCreateHotel: (params) => dispatch(
            receiveResponsePOSTCreateHotel(params)
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateHotel);

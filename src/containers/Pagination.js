import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import  Pagination  from "../components/reusableComponents/Pagination/Pagination.jsx";
import { requestCurrentPage } from "../actions/hotelAction";
import {getArrayFromNumber} from "../helpers";

function mapStateToProps(state) {
  return {
    hotels: state.hotels.listHotls,
    pagesList: getArrayFromNumber(state.hotels.countPage),
    countPage: state.hotels.countPage,
    currentPage: state.hotels.currentPage
  };
}

function mapDispatchToProps(dispatch) {
  return {
    requestCurrentPage: bindActionCreators(requestCurrentPage, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Pagination);

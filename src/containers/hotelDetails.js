import {connect} from 'react-redux';
import {HotelDetails} from '../components/HotelDetails/HotelDetails.jsx';
import {FeedbackSearch} from '../components/HotelDetails/FeedbackSearch.jsx';
import {
    requestGetHotelDetails,
    toggleModal,
    toggleModalCarousel,
    toggleModalFeedback,
} from '../actions/hotelAction';


function mapStateToProps(state) {
    return {
        hotels: state.hotelDetailsReducer.selectedHotel,
        rooms: state.hotelDetailsReducer.selectedHotel.rooms || [],
        photos: state.hotelDetailsReducer.selectedHotel.photos || [],
        address: state.hotelDetailsReducer.selectedHotel.address,
        modal: state.hotelDetailsReducer.modal,
        modalCarousel: state.hotelDetailsReducer.modalCarousel,
        modalFeedback: state.hotelDetailsReducer.modalFeedback,
        authTrue: state.auth.isAuthenticated
    }
};

function mapDispatchToProps(dispatch) {
    return{
        requestGetHotelDetails: (id) => dispatch(requestGetHotelDetails(id)),
        //requestGetFeedbackByHotelId: (id,page) => dispatch(requestGetFeedbackByHotelId(id,page)),
        toggleModal : () => dispatch(toggleModal()),
        toggleModalCarousel: () => dispatch(toggleModalCarousel()),
        toggleModalFeedback: () => dispatch(toggleModalFeedback()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HotelDetails);



import { connect } from 'react-redux';
import ManageHotelComforts from '../components/ManageHotelComforts/ManageHotelComforts.jsx';

// template for future filling:
//
// import {
//     ??
// } from '../actions/?Action';

function mapStateToProps(state) {
    return {
        userId: state.dash.user.id,
        selectedHotel: state.hotelManagementReducer.selectedHotel
    }
}

// template for future filling:
//
// function mapDispatchToProps(dispatch) {
//     return {
//        doSomth: (params) => dispatch(
//            doSometh(params)
//        )
//     }
// }

export default connect(mapStateToProps/*, mapDispatchToProps*/)(ManageHotelComforts);

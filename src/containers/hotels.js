import { connect } from "react-redux";
import { Hotels } from "../components/Hotels/Hotels.jsx";
import { requestGetHotels, requestCurrentPage } from "../actions/hotelAction";
import {
  toggleModal,
} from '../actions/hotelAction';

function mapStateToProps(state, props) {
  return {
    defaultPage: props.defaultPage,
    hotels: state.hotels.listHotls,
    pageSize: state.hotels.pageSize,
    currentPage: state.hotels.currentPage,
    countPage: state.hotels.countPage,
    totalHotelsCount: state.hotels.totalHotelsCount,
    loading: state.hotels.loading,
    modal: state.hotelDetailsReducer.modal,
    authTrue: state.auth.isAuthenticated
  };
}

function mapDispatchToProps(dispatch) {
  return {
    requestGetHotels: () => dispatch(requestGetHotels()),
    requestCurrentPage: (page) => dispatch(requestCurrentPage(page)),
    toggleModal : () => dispatch(toggleModal()),

  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Hotels);
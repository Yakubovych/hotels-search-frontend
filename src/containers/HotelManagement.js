import { connect } from 'react-redux';
import HotelManagement from '../components/HotelManagement/HotelManagement.jsx';
import {
    changeHotelManagementSubPage,
    requestGetListOwnerHotels,
    receiveResponseGetListOwnerHotels
} from '../actions/hotelManagementAction';

function mapStateToProps(state) {
    return {
        userId: state.dash.user.id,
        hotelManagementSubPage: state.hotelManagementReducer.hotelManagementSubPage,
        ownerHotels: state.hotelManagementReducer.ownerHotels
    }
}

function mapDispatchToProps(dispatch) {
    return{
        changeHotelManagementSubPage: (data) => dispatch(
            changeHotelManagementSubPage({
                subPage: data.subPage,
            })
        ),
        requestGetListOwnerHotels: (params) => dispatch(
            requestGetListOwnerHotels(params)
        ),
        receiveResponseGetListOwnerHotels: (params) => dispatch(
            receiveResponseGetListOwnerHotels(params)
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HotelManagement);

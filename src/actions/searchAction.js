import {
    REQUEST_SEARCH_HOTELS,
    RECEIVE_SEARCH_HOTELS,
    REQUEST_SEARCH_TIPS, 
    RECEIVE_SEARCH_TIPS,
    INPUT_SEARCH_STRING,
    INPUT_ROOM_TYPE,
    SELECT_START_DATE,
    SELECT_FINISH_DATE,
    SELECT_ROOM_TYPE,
    CLEAR_SEARCH_INPUTS
} from '../constants/actionTypes';

export const requestSearchHotels = (data) => ({type: REQUEST_SEARCH_HOTELS, payload: data});
export const receiveSearchHotels = (payload) => ({type: RECEIVE_SEARCH_HOTELS, payload});
export const requestSearchTips = (data) => ({type: REQUEST_SEARCH_TIPS, payload: data});
export const receiveSearchTips = (payload) => ({type: RECEIVE_SEARCH_TIPS, payload});
export const inputSearchString = (payload) => ({type: INPUT_SEARCH_STRING, payload});
export const inputRoomType = (payload) => ({type: INPUT_ROOM_TYPE, payload});
export const selectStartDate = (payload) => ({type: SELECT_START_DATE, payload});
export const selectFinishDate = (payload) => ({type: SELECT_FINISH_DATE, payload});
export const selectRoomType = (payload) => ({type: SELECT_ROOM_TYPE, payload});
export const clearSearchInputs = () => ({type: CLEAR_SEARCH_INPUTS}) 
  
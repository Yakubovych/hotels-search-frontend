import {
  REQUEST_GET_HOTELS,
  RECEIVE_GET_HOTELS,
  REQUEST_GET_HOTELDETAILS,
  RECEIVE_GET_HOTELDETAILS,
  REQUEST_GET_FEEDBACK_BY_HOTEL_ID,
  REQUEST_GET_COMFORTS,
  RECEIVE_GET_COMFORTS,
  CLEAR_SEARCH,
  TOGGLE_MODAL,
  REQUEST_CURRENT_PAGE,
  RECEIVE_CURRENT_PAGE,
  TOGGLE_MODAL_CAROUSEL,
  TOGGLE_MODAL_FEEDBACK,
  RECEIVE_GET_FEEDBACK_BY_HOTEL_ID,
  REQUEST_COMFORTS_DELETED,
  RECEIVE_COMFORTS_DELETED,
  RECEIVE_COMFORT_ADDED,
  REQUEST_COMFORT_ADDED
} from "../constants/actionTypes";


export const requestGetHotels = () => ({ type: REQUEST_GET_HOTELS });

export const receiveGetHotels = payload => ({
  type: RECEIVE_GET_HOTELS,
  payload
});

export const requestGetHotelDetails = id => ({
  type: REQUEST_GET_HOTELDETAILS,
  payload: id
});

export const receiveGetHotelDetails = payload => ({
  type: RECEIVE_GET_HOTELDETAILS,
  payload
});

export const requestGetFeedbackByHotelId = (id, page,star) => ({
  type: REQUEST_GET_FEEDBACK_BY_HOTEL_ID,
  payload: {id,page,star}
});

export const receiveGetFeedbackByHotelId = payload => ({
  type: RECEIVE_GET_FEEDBACK_BY_HOTEL_ID,
  payload
});

export const requestGetComforts = id => ({
  type: REQUEST_GET_COMFORTS,
  payload: id
});

export const receiveGetComforts = payload => ({
  type: RECEIVE_GET_COMFORTS,
  payload
});

export const requestDeleteComforts = (room_id, comfort_id)=> ({
  type: REQUEST_COMFORTS_DELETED,
  payload: {room_id, comfort_id}
});

export const receiveDeleteComforts = payload => ({
  type: RECEIVE_COMFORTS_DELETED,
  payload
});

export const requestCreateComfort = (comfort)=> ({
  type: REQUEST_COMFORT_ADDED,
  payload: comfort
});

export const receiveCreateComfort = payload => ({
  type: RECEIVE_COMFORT_ADDED,
  payload
});

export const clearSearch = () => ({ type: CLEAR_SEARCH });

export const toggleModal = () => ({ type: TOGGLE_MODAL });

export const toggleModalCarousel = () => ({ type: TOGGLE_MODAL_CAROUSEL });

export const toggleModalFeedback = () => ({ type: TOGGLE_MODAL_FEEDBACK });

export const requestCurrentPage = payload => ({
  type: REQUEST_CURRENT_PAGE,
  payload
});

export const receiveCurrentPage = payload => ({
  type: RECEIVE_CURRENT_PAGE,
  payload
});

import {
    REQUEST_GET_LIST_ROOMS_IN_ONE_HOTEL,
    RECEIVE_GET_LIST_ROOMS_IN_ONE_HOTEL,
} from '../constants/actionTypes';

export const requestGetListRoomsInHotel = (payload) => ({ 
    type: REQUEST_GET_LIST_ROOMS_IN_ONE_HOTEL, 
    payload
});

export const receiveResponseGetListRoomsInOneHotel = (payload) => ({
    type: RECEIVE_GET_LIST_ROOMS_IN_ONE_HOTEL, 
    payload
});

import {
    RECEIVE_RESPONSE_POST_CREATE_HOTEL,
    REQUEST_POST_CREATE_HOTEL,
} from '../constants/actionTypes';

export const requestPOSTCreateHotel = (hotel) => ({ 
    type: REQUEST_POST_CREATE_HOTEL, 
    payload: hotel 
});

export const receiveResponsePOSTCreateHotel = (payload) => ({
    type: RECEIVE_RESPONSE_POST_CREATE_HOTEL, 
    payload
});

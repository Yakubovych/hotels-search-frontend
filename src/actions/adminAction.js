import {
  REQUEST_GET_INACTIVE_HOTELS,
  REQUEST_UPDATE_STATUS_HOTEL,
  REQUEST_GET_LIST_ACTIVE_HOTELS,
  REQUEST_SEND_OWNER_MESSAGE,
  REQUEST_GET_COUNT_INACTIVE_HOTELS,
  REQUEST_GET_LIST_SUSPEND_HOTELS
} from "../constants/actionTypes";

export const inactiveStatusHotels = () => ({
  type: REQUEST_GET_INACTIVE_HOTELS
});

export const updateStatusHotel = payload => ({
  type: REQUEST_UPDATE_STATUS_HOTEL,
  payload
});

export const getListActiveHolels = () => ({
  type: REQUEST_GET_LIST_ACTIVE_HOTELS
});

export const sendOwnerMessage = payload => ({
  type: REQUEST_SEND_OWNER_MESSAGE,
  payload
});

export const getCountInactiveHotels = () => ({
  type: REQUEST_GET_COUNT_INACTIVE_HOTELS
});

export const getListSuspendHolels = () => ({
  type: REQUEST_GET_LIST_SUSPEND_HOTELS
});
import {
  REQUEST_GET_LIST_ACTIVE_HOTELS
} from "../constants/actionTypes";

export const getListActiveHolels = () => ({
    type: REQUEST_GET_LIST_ACTIVE_HOTELS
});
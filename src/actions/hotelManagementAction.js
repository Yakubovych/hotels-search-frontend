import {
    CHANGE_HOTEL_MANAGEMENT_SUB_PAGE,
    REQUEST_GET_LIST_OWNER_HOTELS,
    RECEIVE_GET_LIST_OWNER_HOTELS  
} from '../constants/actionTypes';

export const changeHotelManagementSubPage = (params) => ({
    type: CHANGE_HOTEL_MANAGEMENT_SUB_PAGE,
    payload: {
        subPage: params.subPage,
        selectedHotel: params.selectedHotel
    }
})

export const requestGetListOwnerHotels = (payload) => ({ 
    type: REQUEST_GET_LIST_OWNER_HOTELS, 
    payload
});

export const receiveResponseGetListOwnerHotels = (payload) => ({
    type: RECEIVE_GET_LIST_OWNER_HOTELS, 
    payload
});



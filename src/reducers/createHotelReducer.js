import {
    RECEIVE_RESPONSE_POST_CREATE_HOTEL,
    REQUEST_POST_CREATE_HOTEL
} from '../constants/actionTypes';

const initialState = {
    hotel: {},
    postResult: null
}

function createHotelReducer(state = initialState, action) {
    switch (action.type) {
        case RECEIVE_RESPONSE_POST_CREATE_HOTEL:
            return {
                ...state,
                postResult: action.payload
            };

        case REQUEST_POST_CREATE_HOTEL:
            return {
                ...state,
                hotel: action.payload
            }
        default:
            return state
    }
}

export default createHotelReducer;

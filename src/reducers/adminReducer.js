import {
  RECEIVE_GET_INACTIVE_HOTELS,
  RECEIVE_UPDATE_STATUS_HOTEL,
  RECEIVE_GET_LIST_ACTIVE_HOTELS,
  RECEIVE_GET_COUNT_INACTIVE_HOTELS,
  RECEIVE_GET_LIST_SUSPEND_HOTELS,
  LOADING,
  REQUEST_GET_LIST_ACTIVE_HOTELS,
  REQUEST_GET_INACTIVE_HOTELS,
  REQUEST_GET_LIST_SUSPEND_HOTELS
} from "../constants/actionTypes";

const initialState = {
  notActiveHotels: [],
  countNotActiveHotels: null,
  listActiveHotels: [],
  countActiveHotels: null,
  countInactiveHotels: null,
  listSuspendHotels: [],
  countSuspendHotels: null,
  loading: false
};

const getUpdatedHotelsList = (hotels, id) =>
  hotels.filter(hotel => hotel.id !== id);

export default function (state = { ...initialState }, action) {
  switch (action.type) {
    case RECEIVE_GET_INACTIVE_HOTELS:
      return {
        ...state,
        notActiveHotels: action.payload.notActiveHotels,
        countNotActiveHotels: action.payload.countNotActiveHotels
      };
    case REQUEST_GET_INACTIVE_HOTELS: {
      return {
        ...state,
        loading: true
      };
    }
    case RECEIVE_GET_LIST_ACTIVE_HOTELS:
      return {
        ...state,
        listActiveHotels: action.payload.listActiveHotels,
        countActiveHotels: action.payload.countActiveHotels
      };
    case REQUEST_GET_LIST_ACTIVE_HOTELS: {
      return {
        ...state,
        loading: true
      };
    }
    case RECEIVE_UPDATE_STATUS_HOTEL:
      const newState = { ...state };
      return {
        newState,
        notActiveHotels: getUpdatedHotelsList(
          newState.notActiveHotels,
          action.payload.activateHotelId
        ),
        listActiveHotels: getUpdatedHotelsList(
          newState.listActiveHotels,
          action.payload.activateHotelId
        ),
        listSuspendHotels: getUpdatedHotelsList(
          newState.listSuspendHotels,
          action.payload.activateHotelId
        ),
        countNotActiveHotels: newState.countNotActiveHotels - 1,
        countActiveHotels: newState.countActiveHotels - 1,
        countInactiveHotels: newState.countInactiveHotels - 1,
        countSuspendHotels: newState.countSuspendHotels - 1
      };
    case RECEIVE_GET_COUNT_INACTIVE_HOTELS:
      return {
        ...state,
        countInactiveHotels: action.payload.countInactiveHotels
      };
    case RECEIVE_GET_LIST_SUSPEND_HOTELS:
      return {
        ...state,
        listSuspendHotels: action.payload.listSuspendHotels,
        countSuspendHotels: action.payload.countSuspendHotels
      };
    case REQUEST_GET_LIST_SUSPEND_HOTELS: {
      return {
        ...state,
        loading: true
      };
    }
    case LOADING: {
      return {
        ...state,
        loading: false
      };
    }
    default:
      return state;
  }
}

import {
    RECEIVE_GET_LIST_ROOMS_IN_ONE_HOTEL
} from '../constants/actionTypes';

const initialState = {
    selectedHotelId: 0,
    selectedHotelRooms: []
}

function manageHotelRoomsReducer(state = initialState, action) {
    switch (action.type) {
        case RECEIVE_GET_LIST_ROOMS_IN_ONE_HOTEL:
            return {
                ...state,
                selectedHotelRooms: action.payload || []
            }
        default:
            return state
    }
}

export default manageHotelRoomsReducer;
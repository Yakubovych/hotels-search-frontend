import {
    CHANGE_HOTEL_MANAGEMENT_SUB_PAGE,
    RECEIVE_GET_LIST_OWNER_HOTELS 
} from '../constants/actionTypes';
import { HOTEL_MANAGEMENT_SUBPAGE, UNKNOWN_HOTEL } from '../constants/appCommonConsts';

const initialState = {
    hotelManagementSubPage: HOTEL_MANAGEMENT_SUBPAGE.DEFAULT,
    selectedHotel: UNKNOWN_HOTEL,
    ownerHotels: []
}

function hotelManagementReducer(state = initialState, action) {
    switch (action.type) {
        case CHANGE_HOTEL_MANAGEMENT_SUB_PAGE:
            return {
                ...state,
                hotelManagementSubPage: action.payload.subPage,
                selectedHotel: action.payload.selectedHotel
            }
        case RECEIVE_GET_LIST_OWNER_HOTELS:
            return {
                ...state,
                ownerHotels: action.payload
            }
        default:
            return state
    }
}

export default hotelManagementReducer;

import { combineReducers } from "redux";
import hotels from "./hotelsReducer";
import { reducer as formReducer } from "redux-form";
import authReducer from "./authReducer";
import dashboardReducer from "./dashboard";
import bookingReducer from "./bookings";
import hotelDetailsReducer from "./hotelDetails";
import createHotelReducer from "./createHotelReducer";
import adminReducer from "./adminReducer";
import upload from './uploadFile';
import feedbacksReducer from "./feedbacksReducer";
import userCabinetOrders from "./userCabinetOrdersReducer";
import hotelManagementReducer from "./hotelManagementReducer";
import manageHotelRoomsReducer from  "./manageHotelRoomsReducer";

const rootReducers = combineReducers({
  bookingReducer,
  hotels,
  hotelDetailsReducer,
  form: formReducer,
  auth: authReducer,
  dash: dashboardReducer,
  createHotelReducer,
  adminReducer,
  upload,
  feedbacksReducer,
  userCabinetOrders,
  hotelManagementReducer,
  manageHotelRoomsReducer
});

export default rootReducers;

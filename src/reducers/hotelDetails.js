import {
  REQUEST_GET_FEEDBACK_BY_HOTEL_ID,
  RECEIVE_GET_FEEDBACK_BY_HOTEL_ID,
  RECEIVE_GET_HOTELDETAILS,
  TOGGLE_MODAL,
  TOGGLE_MODAL_CAROUSEL,
  TOGGLE_MODAL_FEEDBACK
} from "../constants/actionTypes";

const initialState = {
  selectedHotel: {},
  feedbackByHotelId: [],
  feedbackCount: null,
  currentPage: null,
  countPage: null,
  modal: false,
  modalCarousel: false
};

function hotelDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case RECEIVE_GET_HOTELDETAILS:
      return {
        ...state,
        selectedHotel: action.payload
      };
    case TOGGLE_MODAL:
      return {
        ...state,
        modal: !state.modal
      };
    case TOGGLE_MODAL_CAROUSEL:
      return {
        ...state,
        modalCarousel: !state.modalCarousel
      };
    case TOGGLE_MODAL_FEEDBACK:
      return {
        ...state,
        modalFeedback: !state.modalFeedback
      };
    case RECEIVE_GET_FEEDBACK_BY_HOTEL_ID:
      return {
        ...state,
        feedbackByHotelId: action.payload.feedback,
        feedbackCount: action.payload.count
      };
    default:
      return state;
  }
}

export default hotelDetailsReducer;

import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import './manageHotelRooms.scss';
import { MANAGE_HOTEL_ROOMS_MESSAGES } from '../../constants/messages';
import i18n from "i18next";
import { withTranslation } from "react-i18next";

class ManageHotelRooms extends Component {
  static propTypes = {
    onLeaveManageHotelRooms: PropTypes.func,
    requestGetListRoomsInHotel: PropTypes.func,
    selectedHotelId: PropTypes.number,
    userId: PropTypes.number,
    selectedHotelRooms: PropTypes.array,
  }

  static defaultProps = {
    selectedHotelId: 0
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedRoomId: 0,
      selectedRoomComforts: []
    }
  }

  onLeaveManageRooms = () => {
    this.props.onLeaveManageHotelRooms();
  }

  getUniqueKey = () => {
    return Math.random();
  }

  componentDidMount = () => {
    this.props.requestGetListRoomsInHotel(this.props.selectedHotelId);
  }

  renderRoomsList = () => {
    return (
      <Fragment>
        <div className="container">
          <table className="table table-bordered owner-hotel-room-list mt-3">
            <thead className="thead-owner-hotel-room-list">
              <tr>
                {/* <th scope="col" className="text-center">#</th> */}
                <th scope="col" className="text-center">{i18n.t("ROOM_LIST_COLUMNS.ROOM_NUMBER")}</th>
                <th scope="col" className="text-center">{i18n.t("ROOM_LIST_COLUMNS.ROOM_TYPE")}</th>
                <th scope="col" className="text-center">{i18n.t("ROOM_LIST_COLUMNS.COMRORTS")}</th>
              </tr>
            </thead>
            <tbody>
              {!!this.props.selectedHotelRooms.length && (
                (this.props.selectedHotelRooms.map((room, index) => (
                  <tr key={room.id}>
                    {/* <td scope="row" className="td-owner-hotel-rooms-list text-center">{room.id}</td> */}
                    <td className="td-owner-hotel-room-list">{room.roomNumber}</td>
                    <td className="td-owner-hotel-room-list">{room.room_type.name}</td>
                    <td className="td-owner-hotel-room-list">
                      <div key={this.getUniqueKey()} className="col-10">
                        {room.room_comforts.map((item) => (
                          <React.Fragment key={this.getUniqueKey()}>
                            <label key={this.getUniqueKey()} className="room-comfort-label ml-1">
                              <span key={this.getUniqueKey()} className="badge room-comfort-badge">{item.comfort.name}</span>
                            </label>
                          </React.Fragment>
                        ))}
                      </div>
                    </td>
                  </tr>
                ))))}
            </tbody>
          </table>
          <div className="row m-0  justify-content-center">
            {!this.props.selectedHotelRooms.length && (
              <div className="alert alert-danger mt-1" role="alert">
                {MANAGE_HOTEL_ROOMS_MESSAGES.NO_ROOMS}
              </div>
            )}
          </div>
        </div>
      </Fragment>
    );
  }

  render() {
    return (
      <div className="row">
        {this.renderRoomsList()}
      </div>
    )
  }
}

export default withTranslation()(ManageHotelRooms);


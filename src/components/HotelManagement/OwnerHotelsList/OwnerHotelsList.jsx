import React, { Component, Fragment } from 'react';
import i18n from "i18next";
import { withTranslation } from "react-i18next";
import PropTypes from 'prop-types';
import './ownerHotelsList.scss';
import { HOTEL_MANAGEMENT_MESSAGES } from '../../../constants/messages';
import { HOTEL_STATUS } from '../../../constants/EnumForHotelStatus';

class OwnerHotelsList extends Component {
  static propTypes = {
    onClick: PropTypes.func,
    getListOwnerHotels: PropTypes.func,
    ownerHotels: PropTypes.array
  }

  static defaultProps = {
    ownerHotels: []
  }

  constructor(props) {
    super(props);
    this.state = { ownerHotels: [] }
  }

  static getDerivedStateFromProps(nextProps) {
    return {
      ownerHotels: nextProps.ownerHotels
    };
  }


  handleManageHotelButtonClick = (event) => {
    const hotelIndex = event.target.value;
    this.props.onClick({
      selectedHotelId: this.state.ownerHotels[hotelIndex].id,
      subPage: event.target.name
    })
  }

  convertAddressToString = ({ city = '', street = '', house = '' }) => {
    const fullAddress = [city, street, house];
    return fullAddress.join(', ');
  }

  convertHotelStatusToString = (status = "") => {
    return HOTEL_STATUS.hasOwnProperty(status) ? HOTEL_STATUS[status] : "";
  }

  renderTableOwnerHotels = () => {
    return (
      <Fragment>
        <table className="table table-bordered owner-hotels-list mt-3">
          <thead className="thead-owner-hotels">
            <tr>
              <th scope="col" className="text-center">#</th>
              <th scope="col" className="text-center">{i18n.t("OWNER_HOTELS_LIST_COLUMNS.HOTEL")}</th>
              <th scope="col" className="text-center">{i18n.t("OWNER_HOTELS_LIST_COLUMNS.ADDRESS")}</th>
              <th scope="col" className="text-center">{i18n.t("OWNER_HOTELS_LIST_COLUMNS.STATUS")}</th>
              <th scope="col" className="text-center">{i18n.t("OWNER_HOTELS_LIST_COLUMNS.ROOM_DETAILS")}</th>
              <th scope="col" className="text-center">{i18n.t("OWNER_HOTELS_LIST_COLUMNS.ADD_PHOTOS")}</th>
            </tr>
          </thead>
          <tbody>
            {(!!this.state.ownerHotels.length && this.state.ownerHotels.map((hotel, index) => (
              <tr key={hotel.name + hotel.id}>
                <td scope="row" className="td-owner-hotel text-center">{index + 1}</td>
                <td className="td-owner-hotel">{hotel.name}</td>
                <td className="td-owner-hotel">
                  {this.convertAddressToString(hotel.address)}
                </td>
                <td className="td-owner-hotel text-center">
                  {this.convertHotelStatusToString(hotel.status)}
                </td>
                <td className="td-owner-hotel text-center">
                  <button className="btn btn-secondary"
                    onClick={this.handleManageHotelButtonClick} value={index} name="Rooms">
                    {i18n.t("HOTEL_MANAGEMENT.ROOMS")} <span className="badge badge-light">{hotel.rooms.length}</span>
                  </button>
                </td>
                <td className="td-owner-hotel  text-center">
                  <button className="btn btn-secondary"
                    onClick={this.handleManageHotelButtonClick} value={index} name="Photos">
                    {i18n.t("HOTEL_MANAGEMENT.PHOTOS")} <span className="badge badge-light">{hotel.photos.length}</span>
                  </button>
                </td>
              </tr>
            )))}
          </tbody>
        </table>
      </Fragment>
    );
  }

  render() {
    return (
      <>
        {this.renderTableOwnerHotels()}
        {!this.state.ownerHotels.length && (
          <div className="alert alert-secondary mt-1" role="alert">
            {HOTEL_MANAGEMENT_MESSAGES.NO_OWNER_HOTELS}
          </div>
        )}
      </>
    )
  }
}

export default withTranslation()(OwnerHotelsList);

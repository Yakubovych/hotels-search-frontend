import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './hotelManagement.scss';
import CreateHotel from '../../containers/CreateHotel';
import Button from '../reusableComponents/Button';
import OwnerHotelsList from './OwnerHotelsList/OwnerHotelsList.jsx';
import HotelManagementNav from './HotelManagementNav.jsx';
import { HOTEL_MANAGEMENT_SUBPAGE, UNKNOWN_HOTEL } from '../../constants/appCommonConsts';
import ManageHotelRooms from '../../containers/ManageHotelRooms';
import { CREATE_HOTEL_MESSAGES } from '../../constants/messages';
import UploadMultiPhotos from "../../containers/UploadMultiPhotos";

const {
    DEFAULT: SUBPAGE_DEFAULT,
    CREATE_HOTEL: SUBPAGE_CREATE_HOTEL,
    ROOMS: SUBPAGE_ROOMS,
    PHOTOS: SUBPAGE_PHOTOS,
} = HOTEL_MANAGEMENT_SUBPAGE;

class HotelManagement extends Component {
    static propTypes = {
        hotelManagementSubPage: PropTypes.string,
        selectedHotel: PropTypes.object,
        userId: PropTypes.number,
        changeHotelManagementSubPage: PropTypes.func,
        requestGetListOwnerHotels: PropTypes.func,
        ownerHotels: PropTypes.array
    }

    static defaultProps = {
        ownerHotels: []
    }

    constructor(props) {
        super(props);
        this.state = {
            subPage: SUBPAGE_DEFAULT,
            ownerHotels: [],
            selectedHotelId: 0
        }
    }

    static getDerivedStateFromProps(nextProps) {
        return {
            ownerHotels: nextProps.ownerHotels
        };
    }

    componentDidMount = () => {
        this.getListOwnerHotels()
    }

    setSubPage = (subPage, selectedHotelId) => {
        this.props.changeHotelManagementSubPage({ subPage });
        this.setState({ subPage: subPage, selectedHotelId: selectedHotelId });
        this.getListOwnerHotels();
    }

    onLeaveSubPage = () => {
        this.setSubPage(SUBPAGE_DEFAULT);
    }

    handleManageHotelClick = ({ subPage, selectedHotelId }) => {
        this.setSubPage(subPage, selectedHotelId)
    }

    getListOwnerHotels = () => {
        this.props.requestGetListOwnerHotels(this.props.userId);
    }

    renderSubPage = () => {
        const newSubPage = this.state.subPage;
        switch (newSubPage) {
            case SUBPAGE_CREATE_HOTEL: return (
                <CreateHotel
                    onLeaveCreateHotel={this.onLeaveSubPage}
                    userId={this.props.userId}>
                </CreateHotel>
            );
            case SUBPAGE_ROOMS: return (
                <ManageHotelRooms
                    onLeaveManageHotelRooms={this.onLeaveSubPage}
                    selectedHotelId={this.state.selectedHotelId}>
                </ManageHotelRooms>
            );

            case SUBPAGE_PHOTOS: return (
                <UploadMultiPhotos
                    onLeaveUploadMultiPhotos={this.onLeaveSubPage}
                    selectedHotelId={this.state.selectedHotelId}>
                </UploadMultiPhotos>
            );
            default: return (
                <>
                    <div className="row">
                        <div className="col-3 m-0">
                            <Button
                                type='button'
                                label={CREATE_HOTEL_MESSAGES.CREATE_HOTEL}
                                className='btn ch-btn-main-success create-hotel-button m-0'
                                handleClick={() => this.setSubPage(SUBPAGE_CREATE_HOTEL)}
                            />
                        </div>
                    </div>
                    <OwnerHotelsList
                        onClick={this.handleManageHotelClick}
                        ownerHotels={this.state.ownerHotels}>
                    </OwnerHotelsList>
                </>
            );
        }
    }

    renderNav = () => {
        const { selectedHotel = UNKNOWN_HOTEL } = this.state;
        const subPathName = selectedHotel.name !== '' ? `${selectedHotel.name}: ${this.state.subPage}` : this.state.subPage;
        return (
            <HotelManagementNav
                subPathName={subPathName}
                onClick={() => this.setSubPage(SUBPAGE_DEFAULT)}>
            </HotelManagementNav>
        );
    }

    render() {
        return (
            <div className="container">
                {this.renderNav()}
                {this.renderSubPage()}
            </div>
        )
    }
}

export default HotelManagement;



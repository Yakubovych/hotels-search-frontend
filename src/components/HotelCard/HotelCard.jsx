import React from "react";
import { Link } from "react-router-dom";
import StarRatings from "react-star-ratings";
import PropTypes from "prop-types";
import {
  STAR_RATING_COLOR,
  STAR_RATING_DIMENSION,
  STAR_NUMBER
} from "../../constants/actionTypes";
import Button from "../reusableComponents/Button";
import { getMainPhotoUrl } from "../../helpers";
import { DEFAULT_HOTEL_IMAGE } from "../../constants";
import Modal from "react-modal";
import BookingWrapper from "../BookingForm/BookingWrapper.jsx";
import i18n from "i18next";
import { withTranslation } from "react-i18next";

const HotelCard = props => {
  return (
    <div className="hotel-card-box position-relative">
      <img
        className="hotel-main-img w-100"
        src={
          props.hotel.photos[0] !== undefined
            ? getMainPhotoUrl(props.hotel.photos)
            : DEFAULT_HOTEL_IMAGE
        }
        alt={props.hotel.name}></img>
      <div
        className="hotel-card-info position-absolute w-100 h-100 text-center
                    		d-flex-column-between">
        <div className="hotel-title w-100 p-3">
          <h4 className="hotel-card-title m-0 text-uppercase">
            {props.hotel.name}
          </h4>
          <StarRatings
            rating={props.hotel.starRating}
            starRatedColor={STAR_RATING_COLOR}
            numberOfStars={STAR_NUMBER}
            starDimension={STAR_RATING_DIMENSION}
            name="rating"
          />
        </div>
        <Button
          type="submit"
          label={
            <Link className="" to={`/hotels/${props.hotel.id}`}>
              More info
            </Link>
          }
          className="btn btn-outline-light opacity hotel-btn"
        />
          {props.info.authTrue ? (
            <Button
              type="submit"
              label={i18n.t("Book the room")}
              className="btn ch-btn-book p-4  mb-4 opacity"
              handleClick={props.info.toggleModal}
            />
          ) : null}

          <Modal
            isOpen={props.info.modal}
            onRequestClose={props.info.toggleModal}
            className="modal-styling">
            <BookingWrapper
              onRequestClose={props.info.toggleModal}
              dataInfo={props.hotel}
            />
          </Modal>
      
      
        <h3 className="hotel-card-adrress w-100 m-0 p-3 text-uppercase">
          {props.hotel.address.city}
        </h3>
      </div>
    </div>
  );
};

HotelCard.propTypes = {
  hotel: PropTypes.object,
  info: PropTypes.object
};

export default HotelCard;
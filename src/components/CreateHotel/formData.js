export const formFields = [
    {
        container: {
            key: "containerHotelName",
            className: "col-lg-6 mt-2",
        },
        label: {
            key: "labelHotelName",
            labelText: "Hotel name:"
        },
        editableField: {
            key: "inputHotelName",
            tagName: "input",
            className: "input-field",
            elementType: "text",
            elementName: "hotelName",
            placeholder: "Enter your hotel name...",
            readOnly: false
        },
        errorMessage: {
            key: "divHotelName",
            constName: "hotelName"
        }
    },
    {
        container: {
            key: "containerHotelDescription",
            className: "col-lg-6 mt-2",
        },
        label: {
            key: "labelHotelDescription",
            labelText: "Hotel description:"
        },
        editableField: {
            key: "inputHotelDescription",
            tagName: "textarea",
            className: "hotel-description",
            rows: 3,
            elementType: "text",
            elementName: "hotelDescription",
            placeholder: "Enter your hotel description...",
        },
        errorMessage: {
            key: "divHotelDescription",
            constName: "hotelDescription"
        }
    },   
    {
        container: {
            key: "containerHotelStarRating",
            className: "col-lg-12 mt-2",
        },
        label: {
            key: "labelHotelStarRating",
            labelText: "Hotel star rating:"
        },
        editableField: {
            tagName: "StarRatingComponent",
            className: "starRating",
            elementName: "hotelStarRating",
            starCount: 5,
            value: "starRating",
            onClick: "onStarClick"
        }
    },
    {
        container: {
            key: "containerCountry",
            className: "col-lg-6 mt-2",
        },
        label: {
            key: "labelCountry",
            labelText: "Country:"
        },
        editableField: {
            key: "inputCountry",
            tagName: "input",
            className: "input-field",
            elementType: "text",
            elementName: "country",
            placeholder: "Enter country...",
            readOnly: false
        },
        errorMessage: {
            key: "divCountry",
            constName: "country"
        }
    },
    {
        container: {
            key: "containerCity",
            className: "col-lg-6 mt-2",
        },
        label: {
            key: "labelCity",
            labelText: "City:"
        },
        editableField: {
            key: "inputCity",
            tagName: "input",
            className: "input-field",
            elementType: "text",
            elementName: "city",
            placeholder: "Enter city...",
            readOnly: false
        },
        errorMessage: {
            key: "divCity",
            constName: "city"
        }
    },
    {
        container: {
            key: "containerStreet",
            className: "col-lg-6 mt-2",
        },
        label: {
            key: "labelStreet",
            labelText: "Street:"
        },
        editableField: {
            key: "inputStreet",
            tagName: "input",
            className: "input-field",
            elementType: "text",
            elementName: "street",
            placeholder: "Enter street...",
            readOnly: false
        },
        errorMessage: {
            key: "divStreet",
            constName: "street"
        }
    },
    {
        container: {
            key: "containerHouse",
            className: "col-lg-6 mt-2",
        },
        label: {
            key: "labelHouse",
            labelText: "House:"
        },
        editableField: {
            key: "inputHouse",
            tagName: "input",
            className: "input-field",
            elementType: "text",
            elementName: "house",
            placeholder: "Enter house number...",
            readOnly: false
        },
        errorMessage: {
            key: "divHouse",
            constName: "house"
        }
    },
    {
        container: {
            key: "containerLatLong",
            className: "col-lg-6 mt-2",
        },
        label: {
            key: "labelLatLong",
            labelText: "Google map coordinates (lat, long):"
        },
        editableField: {
            key: "inputLatLong",
            tagName: "input",
            className: "input-field",
            elementType: "text",
            elementName: "latLong",
            placeholder: "Enter latitude and longitude...",
            readOnly: true
        },
        errorMessage: {
            key: "divLatLong",
            constName: "latLong"
        }
    }
];

export const formButtons = [
    {
        wrapperKey: "divCHClose",
        button: {
            key: "buttonClose",
            name: "close",
            label: "Close",
            className: "btn btn-main-danger text-white create-hotel-button",
            handleClick: "canLeaveForm"
        }
    },
    {
        wrapperKey: "divCHSubmit",
        button: {
            key: "buttonSubmit",
            name: "submit",
            label: "Submit",
            className: "btn ch-btn-primary text-white create-hotel-button",
            handleClick: "createHotel"
        }
    }
];
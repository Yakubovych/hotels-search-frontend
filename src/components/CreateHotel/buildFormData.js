export const MAX_STAR_COUNT = 5;
export const COMMON_FIELD_DATA = {
    tagName: {
        input: 'input',
        starRatingComponent: 'StarRatingComponent',
        textarea: 'textarea'
    },
    className: {
        fieldLabel: 'create-hotel-input-field-label',
        divInvalidField: 'div-invalid-field'
    }
}

export const FORM_FIELDS = [
    {
        container: {
            key: 'containerHotelName',
            className: 'col-lg-6 mt-2',
        },
        label: {
            key: 'labelHotelName',
            labelText: 'Hotel name:'
        },
        editableField: {
            key: 'inputHotelName',
            tagName: COMMON_FIELD_DATA.tagName.input,
            className: 'input-field',
            elementType: 'text',
            elementName: 'name',
            placeholder: 'Enter your hotel name...',
            readOnly: false
        },
        errorMessage: {
            key: 'divHotelName',
            constName: 'name'
        }
    },
    {
        container: {
            key: 'containerHotelDescription',
            className: 'col-lg-6 mt-2',
        },
        label: {
            key: 'labelHotelDescription',
            labelText: 'Hotel description:'
        },
        editableField: {
            key: 'inputHotelDescription',
            tagName: COMMON_FIELD_DATA.tagName.textarea,
            className: 'hotel-description',
            rows: 3,
            elementType: 'text',
            elementName: 'description',
            placeholder: 'Enter your hotel description...',
        },
        errorMessage: {
            key: 'divHotelDescription',
            constName: 'description'
        }
    },
    {
        container: {
            key: 'containerHotelStarRating',
            className: 'col-lg-12 mt-2 star-rating-container',
        },
        label: {
            key: 'labelHotelStarRating',
            labelText: 'Hotel star rating:'
        },
        editableField: {
            tagName: COMMON_FIELD_DATA.tagName.starRatingComponent,
            className: 'starRating',
            elementName: 'hotelStarRating',
            starCount: MAX_STAR_COUNT,
            value: 'starRating',
            onClick: 'onStarClick'
        }
    },
    {
        container: {
            key: 'containerCountry',
            className: 'col-lg-6 mt-2',
        },
        label: {
            key: 'labelCountry',
            labelText: 'Country:'
        },
        editableField: {
            key: 'inputCountry',
            tagName: 'input',
            className: 'input-field',
            elementType: 'text',
            elementName: 'country',
            placeholder: 'Enter country...',
            readOnly: false
        },
        errorMessage: {
            key: 'divCountry',
            constName: 'country'
        }
    },
    {
        container: {
            key: 'containerCity',
            className: 'col-lg-6 mt-2',
        },
        label: {
            key: 'labelCity',
            labelText: 'City:'
        },
        editableField: {
            key: 'inputCity',
            tagName: 'input',
            className: 'input-field',
            elementType: 'text',
            elementName: 'city',
            placeholder: 'Enter city...',
            readOnly: false
        },
        errorMessage: {
            key: 'divCity',
            constName: 'city'
        }
    },
    {
        container: {
            key: 'containerStreet',
            className: 'col-lg-6 mt-2',
        },
        label: {
            key: 'labelStreet',
            labelText: 'Street:'
        },
        editableField: {
            key: 'inputStreet',
            tagName: COMMON_FIELD_DATA.tagName.input,
            className: 'input-field',
            elementType: 'text',
            elementName: 'street',
            placeholder: 'Enter street...',
            readOnly: false
        },
        errorMessage: {
            key: 'divStreet',
            constName: 'street'
        }
    },
    {
        container: {
            key: 'containerHouse',
            className: 'col-lg-6 mt-2',
        },
        label: {
            key: 'labelHouse',
            labelText: 'House:'
        },
        editableField: {
            key: 'inputHouse',
            tagName: COMMON_FIELD_DATA.tagName.input,
            className: 'input-field',
            elementType: 'text',
            elementName: 'house',
            placeholder: 'Enter house number...',
            readOnly: false
        },
        errorMessage: {
            key: 'divHouse',
            constName: 'house'
        }
    }
];

export const FORM_BUTTONS = [
    {
        wrapperKey: 'wrapper_divClose',
        button: {
            key: 'buttonClose',
            name: 'close',
            label: 'Close',
            className: 'btn ch-btn-danger create-hotel-button',
            handleClick: 'canLeaveForm',
        }
    },
    {
        wrapperKey: 'wrapper_divSubmit',
        button: {
            key: 'buttonSubmit',
            name: 'submit',
            label: 'Submit',
            className: 'btn ch-btn-success create-hotel-button',
            handleClick: 'createHotel',
        }
    }
];

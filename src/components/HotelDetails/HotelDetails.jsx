import React, { Component} from "react";
import BookingWrapper from "../BookingForm/BookingWrapper.jsx";
import Modal from "react-modal";
import PropTypes from "prop-types";
import { HotelCarousel } from "./HotelCarousel.jsx";
import  FeedbackSearch  from "../../containers/FeedbackSearch";
import Button from "../reusableComponents/Button";
import Map from "../reusableComponents/Map/Map.jsx";
import {STAR_NUMBER, STAR_RATING_COLOR, ZOOM} from "../../constants/actionTypes";
import i18n from 'i18next';
import { withTranslation  } from 'react-i18next';
import {FaMapMarkedAlt} from 'react-icons/fa';
import StarRatings from "react-star-ratings";
import {getMainPhotoUrl} from "../../helpers";
import {DEFAULT_HOTEL_URL} from "../../constants";
import { TinyButton as ScrollUpButton } from "react-scroll-up-button";



export class HotelDetails extends Component {
    constructor(props) {
        super(props);
    }

    onHandleChange(state){
        this.setState(state)
    }

    closeCarouselModal(){
        this.props.toggleModalCarousel();
    }

    closeFeedbackModal(){
        this.props.toggleModalFeedback();
    }

    componentDidMount() {
        const id = parseInt(this.props.match.params.id);
        this.props.requestGetHotelDetails(id);
    }

    renderHotelAddress(address) {
        return (
            <span className='m-0'>
                {address.street},{address.house},{address.city},{address.country}
            </span>
        );
    }

    render() {
        let coordinates = this.props.hotels.address &&
            this.props.hotels.address.latLong.split(',');
        return (
            <div>
                <ScrollUpButton  ContainerClassName='scroll-up-ch'/>
            {this.props.hotels && this.props.photos && (
                <div className='hotel-detail container-fluid'>
                    <div className='row' onClick={this.props.toggleModalCarousel}>
                        <img
                            className='hotel-picture'
                            src={
                                this.props.hotels.photos !== undefined
                                    ? getMainPhotoUrl(this.props.hotels.photos)
                                    : DEFAULT_HOTEL_URL
                            }
                            alt={this.props.hotels.name}
                        />
                        <div className="open-carousel">
                            <p>View photos</p>
                        </div>
                    </div>
                    <div className="row hotel-info">
                        <div className="col-sm col-md-8 col-lg-8 hotel-info-color p-5">
                            <div className="d-flex justify-content-between">
                                <h3 className="hotel-name">
                                    {this.props.hotels.name}
                                </h3>
                                { (this.props.authTrue)
                                    ? <Button
                                        type='submit'
                                        label={i18n.t('BOOK THE ROOM')}
                                        className ='ch-btn-main-color w-25 p-2 '
                                        handleClick={this.props.toggleModal}
                                    />
                                    : null
                                }
                                <Modal
                                    isOpen={this.props.modal}
                                    onRequestClose={this.props.toggleModal}
                                    className='modal-styling'
                                >
                                    <BookingWrapper
                                        onRequestClose={this.props.toggleModal}
                                        dataInfo={this.props.hotels}
                                    />
                                </Modal>
                            </div>
                            <StarRatings
                                rating={this.props.hotels.starRating}
                                starRatedColor={STAR_RATING_COLOR}
                                numberOfStars={STAR_NUMBER}
                                starDimension="22px"
                            />
                            <h5 className="m-2">
                                <FaMapMarkedAlt/>  {this.props.address &&
                                this.renderHotelAddress(this.props.address)}
                            </h5>
                            <hr/>
                            <p className="text-justify">{this.props.hotels.description}</p>
                            <hr/>
                            {this.props.hotels.address &&
                            (
                                <Map className="googleMap"
                                     key={this.props.hotels.id}
                                     mapGetStateToParent={this.onHandleChange.bind(this)}
                                     mapState={this.state}
                                     google={this.props.google}
                                     center={{ lat: parseFloat(coordinates[0]),
                                               lng: parseFloat(coordinates[1]) }}
                                     height='250px'
                                     zoom={ZOOM}
                                     readOnly={false}
                                     autocomplete={false}
                                />
                            )}
                        </div>
                    </div>
                    <div className="row my-5">
                        {this.props.rooms &&  this.props.rooms.map((room,index) =>
                                <div className="col-6 card roomDetails align-items-center" key={Math.floor(Math.random()*1000000)}>
                                    <h2 className="mt-4 font-weight-bold">{room.room_type.name}</h2>
                                    <h3 className="mt-2 font-weight-bold">{room.price}₴</h3>
                                    <div className="card-body" >
                                        <ul className="card-text">
                                            {room.room_comforts && room.room_comforts.map(comforts =>
                                                (<React.Fragment key={Math.floor(Math.random()*1000000)}>
                                                    <li className="comfort-list">{comforts.comfort.name}</li>
                                                </React.Fragment>)
                                            )}
                                        </ul>
                                    </div>

                                { (this.props.authTrue)
                                    ? <Button
                                        type='submit'
                                        label={i18n.t('BOOK THE ROOM')}
                                        className ='ch-btn-main-color py-2 px-4 my-3'
                                        handleClick={this.props.toggleModal}
                                    />
                                    : null
                                }
                                <Modal
                                    isOpen={this.props.modal}
                                    onRequestClose={this.props.toggleModal}
                                    className='modal-styling'
                                >
                                    <BookingWrapper
                                        onRequestClose={this.props.toggleModal}
                                        dataInfo={this.props.hotels}
                                    />
                                </Modal>
                            </div>
                        )}

                        <div className="card col-6 w-23 align-items-center">
                            <div className="card-body">
                                <p className="card-title reviewNumber p-4">
                                    {parseFloat(this.props.hotels.reviewsRating).toFixed(1)}
                                </p>
                                <p className="">total reviews rating</p>
                                <Button
                                    type='redirect'
                                    label='View feedback'
                                    className ='ch-btn-black py-2 px-4'
                                    handleClick={this.props.toggleModalFeedback}
                                />
                                <Modal
                                    isOpen={this.props.modalFeedback}
                                    onRequestClose={this.props.toggleModalFeedback}
                                    className="col-10 ml-5 feedback-modal"
                                >
                                    <FeedbackSearch hotelId={this.props.hotels.id}
                                                    onRequestClose={this.closeFeedbackModal.bind(this)}
                                    />
                                </Modal>
                            </div>
                        </div>
                    </div>
                    <div className='col carousel'>
                        <Modal
                            isOpen={this.props.modalCarousel}
                            className="carousel-modal"
                        >
                        {this.props.photos && this.props.hotels.id && (
                            <HotelCarousel
                                key={this.props.hotels.id}
                                images={this.props.photos}
                                onRequestClose={this.closeCarouselModal.bind(this)}
                            />
                        )}
                        </Modal>
                    </div>
                </div>
                )}
            </div>
        );
    }
}

HotelDetails.propTypes = {
    hotels: PropTypes.object,
    rooms: PropTypes.array,
    photos: PropTypes.array,
    address: PropTypes.object,
    requestGetHotelDetails: PropTypes.func,
    requestGetFeedbackByHotelId: PropTypes.func,
    google: PropTypes.any,
    match: PropTypes.any,
    authTrue:  PropTypes.bool,
    toggleModal: PropTypes.any,
    modal: PropTypes.any,
    toggleModalCarousel: PropTypes.any,
    toggleModalFeedback: PropTypes.any,
    modalCarousel: PropTypes.any,
    modalFeedback: PropTypes.any,
    feedback: PropTypes.array,
    feedbackCount: PropTypes.any,
    countPage: PropTypes.any
};

export default withTranslation() (HotelDetails);

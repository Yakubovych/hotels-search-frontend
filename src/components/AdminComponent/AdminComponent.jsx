import React from "react";
import PropTypes from "prop-types";
import ListHotels from "./ListHotels/ListHotels.jsx";
import { STATUS_HOTELS } from "../../constants/actionTypes";
import { TinyButton as ScrollUpButton } from "react-scroll-up-button";

export class AdminComponent extends React.Component {
  constructor(props) {
    super(props);
    this.showMyTab = this.showMyTab.bind(this);
    this.state = {
      component: STATUS_HOTELS.INACTIVE
    };
  }

  showMyTab(component) {
    this.setState({
      component: component
    });
  }

  render() {
    return (
      <>
       <ScrollUpButton  ContainerClassName='scroll-up-ch'/>
      <div className="container-fluid">
        <ul className="nav nav-tabs" id="myTab" role="tablist">
          <li className="nav-item">
            <a
              className="nav-link p-2 active"
              id="created-orders"
              data-toggle="tab"
              href="#home"
              role="tab"
              aria-controls="home"
              aria-selected="true"
              onClick={() => this.showMyTab(STATUS_HOTELS.INACTIVE)}
            >
              Inactive hotels
            </a>
          </li>
          <li className="nav-item">
            <a
              className="nav-link p-2"
              id="paid-orders"
              data-toggle="tab"
              href="#profile"
              role="tab"
              aria-controls="profile"
              aria-selected="false"
              onClick={() => this.showMyTab(STATUS_HOTELS.ACTIVE)}
            >
              Active hotels
            </a>
          </li>
          <li className="nav-item">
            <a
              className="nav-link p-2"
              id="completed-orders"
              data-toggle="tab"
              href="#contact"
              role="tab"
              aria-controls="contact"
              aria-selected="false"
              onClick={() => this.showMyTab(STATUS_HOTELS.SUSPEND)}
            >
              Suspend hotels
            </a>
          </li>
        </ul>
        <div className="tab-content" id="myTabContent">
          <div
            className="tab-pane fade show active"
            id="home"
            role="tabpanel"
            aria-labelledby="created-orders"
          >
            {this.state.component == STATUS_HOTELS.INACTIVE ? (
              <ListHotels
                status={STATUS_HOTELS.INACTIVE}
                listHotel={this.props.notActiveHotels}
                updateStatusHotel={this.props.updateStatusHotel}
                loading={this.props.loading}
                data={this.props.inactiveStatusHotels}
              />
            ) : null}
            {this.state.component == STATUS_HOTELS.ACTIVE ? (
              <ListHotels
                status={STATUS_HOTELS.ACTIVE}
                listHotel={this.props.listActiveHotels}
                updateStatusHotel={this.props.updateStatusHotel}
                loading={this.props.loading}
                data={this.props.getListActiveHolels}
              />
            ) : null}
            {this.state.component == STATUS_HOTELS.SUSPEND ? (
              <ListHotels
                status={STATUS_HOTELS.SUSPEND}
                listHotel={this.props.listSuspendHotels}
                updateStatusHotel={this.props.updateStatusHotel}
                loading={this.props.loading}
                data={this.props.getListSuspendHolels}
              />
            ) : null}
          </div>
        </div>
      </div>
      </>
    );
  }
}

AdminComponent.propTypes = {
  inactiveStatusHotels: PropTypes.func,
  notActiveHotels: PropTypes.array,
  countNotActiveHotels: PropTypes.number,
  updateStatusHotel: PropTypes.func,
  listActiveHotels: PropTypes.array,
  getListActiveHolels: PropTypes.func,
  status: PropTypes.string,
  getCountInactiveHotels: PropTypes.func,
  getListSuspendHolels: PropTypes.func,
  listSuspendHotels: PropTypes.array,
  loading:  PropTypes.bool
};